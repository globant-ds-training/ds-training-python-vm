#!/bin/bash

# First update the system
echo ""
echo ""
echo "Updating the base system..."
CONTROL=1
sudo apt-get update
until [ $CONTROL -eq 0 ]; do
    sudo apt-get -y dist-upgrade
    CONTROL=$?
done

# Install several useful packages
echo ""
echo ""
echo "Installing extra packages..."
CONTROL=1
until [ $CONTROL -eq 0 ]; do
    sudo apt-get install -y git subversion curl x11-apps
    CONTROL=$?
done

# Add the following to file .bashrc to enable a custom prompt
cat <<EOT >> /home/vagrant/.bashrc
# Add a custom color prompt
CUSTOM_PROMPT="~"
bldylw='\e[1;33m'
bldgrn='\e[1;32m'
bldblu='\e[1;34m'
txtrst='\e[0m'
print_before_the_prompt () {
printf "\$bldylw[ \$bldgrn%s@%s: \$bldblu%s \$bldylw]\n\$txtrst" "\$USER" "\$HOSTNAME" "\${PWD/\$HOME/\$CUSTOM_PROMPT}"
}
PROMPT_COMMAND=print_before_the_prompt
PS1='~> '
EOT

# Install Anaconda Python
echo ""
echo ""
echo "Installing Anaconda Python 3..."
CONTROL=1
until [ $CONTROL -eq 0 ]; do
    wget --progress=bar:force https://repo.continuum.io/archive/Anaconda3-4.3.1-Linux-x86_64.sh
    CONTROL=$?
done
bash /home/vagrant/Anaconda3-4.3.1-Linux-x86_64.sh -b -p /home/vagrant/anaconda3

cat <<EOT >> /home/vagrant/.bash_aliases
# Anaconda 3 aliases
CONDA_HOME=\$HOME/anaconda3/
alias activate='source \$CONDA_HOME/bin/activate'
alias deactivate='source \$CONDA_HOME/bin/deactivate'
alias conda='\$CONDA_HOME/bin/conda'
EOT

echo ""
echo ""
echo "Checking for available Anaconda updates..."
/home/vagrant/anaconda3/bin/conda update -y conda

cat <<EOT >> /home/vagrant/.bashrc
export PATH=/home/vagrant/anaconda3/bin:$PATH
EOT

# Create a base environment with packages necessary for running Rodeo
# Also create necessary configuration for running Jupyter
/home/vagrant/anaconda3/bin/conda create -y --name datasci numpy scipy pandas matplotlib jupyter
source /home/vagrant/anaconda3/bin/activate datasci
pip install ggplot

sudo mkdir /home/vagrant/.jupyter
jupyter notebook --generate-config

cat <<EOT >>/home/vagrant/.jupyter/jupyter_notebook_config.json
{
  "NotebookApp": {
    "password": "sha1:347728aa1e32:fab6561af0be7d6f24f35465fb38c464cf3b0535"
  }
}
EOT

source /home/vagrant/anaconda3/bin/deactivate

# Install the Rodeo IDE
echo ""
echo ""
echo "Installing Rodeo..."
cat <<EOT >> /home/vagrant/key.txt
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: SKS 1.1.5
Comment: Hostname: pgp.mit.edu

mQENBFXoVD0BCADV1hQAs5ABYwmcL3HLSLy5+Ygm62rEATeF4Q7AupdgbQ1XaKxP1YjUoK63
1OUb70EC3YC26BsuV2DGH9WD/uUAk/A6+9vuSIZj/wJKUYSvZYm2QRUPenX7/wOdRj6VWC44
k/HJqzYH7B+WxGomCDyw2vLK86sVTsjGtqGruFHTMs7lsLP8FyMIsVMQKcYdfo5ZqVLvob+Z
MKbbEgJvqbaSowWW+/2TQECBZQYQJRJLwQ+QmyY4w15fUOQ3Qt0Y1Sn/Y00iw/6MwD05eKIF
YjUVYJCvyfHgXbo5nx+AO8J1KJVkU890Mlyy5M+/fNJKbXGuW0ZXs4nj1EmPvl6SgYtRABEB
AAG0GVloYXQgRGV2IDxkZXZAeWhhdGhxLmNvbT6JATgEEwECACIFAlXoVD0CGwMGCwkIBwMC
BhUIAgkKCwQWAgMBAh4BAheAAAoJEFL4ol8z1AvGjFcIAIhhzMm3kgm3l5AJrge8noXQ4+tl
S2YRdP4pmS0wgHgd3KAoqQUAbwKBJwzR6rZQEZW1oGLCAL9aRh1EqBugMuw67F/wzcW6LCQ+
cegjTknYezUoAnA8nSBBly/bu6CimzDoOim1s9XVP3cjmkEmdh8Ln8DmdhPR4H9lbzaHJ2N8
db19mDtjlfrnKmqBRNOev+IMRqPNQvGYdA4PhOBzyG/UIzGsufA6oweYsdiapYJ8K4/ufe7E
SS6VWZnLGgZnybAPjuHdZ7vYNgN/N7HbbP/J0qPSWZPqbmIQWl/yiUuj6voIAvwS+82C9e5a
TCiGXd9rJ1KDj74g4qSKX1HV6QW5AQ0EVehUPQEIAMgYbrsm0GKyTDGCh3IDa11gC6SZ4Adw
BSYtDo8ewo5GXtYrcfCZMAtW0NaJX4et5sQhlzhlYzSh67/ydnTr9vgazmhuMG2Cv53Qpwvx
zKH1JF+3SdP6XIHJAAz+QQfghvN2IPUE1uUqMemYxWNFqHOplQQHX2FVyVaY7hapTbZkA/Xr
9/SkOZuUM8kKLRJETO9ylrKI+p9ZIW7Mt128llQcpl5c2b0tiIvHQaNWMy/dfM2ZvcYSlEZp
CKy+c9SgU/BRNG7U2Oo6jRWBl4fm/krOyH5xrztxNwZf96hK8daNILyFYN6kgvPvMl6BPoxP
6HLwfVodq7uS0GDPE1wDL3UAEQEAAYkBHwQYAQIACQUCVehUPQIbDAAKCRBS+KJfM9QLxoYp
B/9SDGGXqEt6UDhH6EGBe2EFie9VKfzzX+pcvMSZwSyfVYUjkTBlJ/IiW1SlJOyuvB5qBNhD
iO+GCNabfSRZo+sgM9cHYEq/8VELJ/eZ0k0V/SumRAqL65B+XjCX92o5LrcxCZJXs7fj7Zg7
OeP34IRFJY2WPpbosjP8njJCqWCgyOhLi/OhiTVRbYJhYFL175HMROhGJ4PNRBHDKug3JTt2
NjwiXRwM7VQUSjIFfiMWT2b+NHgrAcfTtFRXmH2A2J7vYXHUY/xYxUNPTWIxpEfFwVGoj5uF
VqSEaC4XAG2AgKUXrC0zByvyEkLAAyiEGVd1LmjcN8qR8jBfRGTo7HdS
=kfog
-----END PGP PUBLIC KEY BLOCK-----
EOT
sudo apt-key add key.txt
sudo add-apt-repository "deb http://rodeo-deb.yhat.com/ rodeo main"

CONTROL=1
sudo apt-get update
until [ $CONTROL -eq 0 ]; do
    sudo apt-get install -y libxss1 libgconf2-4 libnss3 libcanberra-gtk*
    CONTROL=$?
done

CONTROL=1
until [ $CONTROL -eq 0 ]; do
    sudo apt-get install -y rodeo
    CONTROL=$?
done

cat <<EOT >> /home/vagrant/.bash_aliases
# Alias for Rodeo
alias rodeo='/opt/Rodeo/rodeo'
EOT

sudo chown -R vagrant.vagrant /home/vagrant/

# Experimental: install JupyterLab
echo ""
echo ""
echo "Installing JupyterLab (Alpha) to the datasci environment..."
source /home/vagrant/anaconda3/bin/activate datasci
conda install -y -c conda-forge jupyterlab
source /home/vagrant/anaconda3/bin/deactivate

# Delete downloaded installation packages and temp files
echo ""
echo ""
echo "Cleaning up..."
rm /home/vagrant/Anaconda3*.sh /home/vagrant/key.txt
sudo apt-get -y autoremove


echo ""
echo ""
echo "Provisioning complete."
