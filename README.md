# Ubuntu 14.04 VM with Anaconda Python

## Useful Vagrant plugins

To automatically set up guest additions for VirtualBox, install

``` bash
vagrant plugin install vagrant-vbguest
```

If you are behind a proxy, the `vagrant-proxyconf` plugin will help you set up
the network connection properly. Install it with

``` bash
vagrant plugin install vagrant-proxyconf
```

## Configure the amount of RAM

To change the amount of RAM allocated to the VM, locate the following block in
the `Vagrantfile`:

``` ruby
config.vm.provider "virtualbox" do |vb|
  # Display the VirtualBox GUI when booting the machine
  # vb.gui = true

  # Customize the amount of memory on the VM:
  vb.memory = "4096"
end
```

Change the value of `vb.memory` to something appropriate for your system.


## Run a Jupyter server

To start a Jupyter server, first log into the VM with

``` bash
vagrant up
```

Anaconda Python 3 is already installed. There is already an environment called
`datasci` with NumPy, SciPy, pandas, matplotlib and Jupyter in it, which you can
use (or you can create your own environment instead). To activate this
environment, run

``` bash
activate datasci
```

(this is an alias for `source activate datasci`). You can then run Jupyter with

``` bash
jupyter notebook --no-browser --ip=0.0.0.0
```

This will start a server at `localhost:8888`. The VM is already configured to
forward its port 8888 to the same port in the host machine.

To stop the server, press Ctrl+C and to deactivate the `datasci` environment,
run

``` bash
deactivate
```

which is an alias for `source deactivate`.


## Run Jupyterlab

To use JupyterLab instead, run the following:

``` bash
jupyter lab --no-browser --ip=0.0.0.0
```

This will start a JupyterLab server, also running at `localhost:8888`.

## Shut down

To shut down the VM, first log off, then run

``` bash
vagrant halt
```
